import pandas as pd
import numpy as np
import itertools
from datetime import datetime
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.preprocessing import normalize
import matplotlib.pyplot as plt

# Variables 
reduced_components = 19
threshold = 10
n_clusters = 4

# Load in data
stations = pd.read_csv("cycledata_LDN_BikePoints.csv")

checkins = pd.read_csv("2015_01-02.csv")

# Remove any erroneous entries
checkins = checkins[checkins["StartStationID"] != checkins["EndStationID"]]
checkins = checkins[checkins["EndStationID"] != -1]

# Not needed but interesting to see the types of station according to mixture model analysis
clustered_stations = pd.read_csv("stations_clustered.csv")

# Get the number of checkins at each station
def get_station_counts(checkins, stations):
    return stations.venueID.map(lambda id: len(checkins[(checkins["StartStationID"] == id) | (checkins["EndStationID"] == id)]))

# Remove stations with 0 checkins
def remove_unused_stations(checkins, stations):
    stations["count"] = get_station_counts(checkins, stations)
    return stations[stations["count"] != 0]

# Get all the possible combination counts
def pair_data(stations, checkins):
    stationCount = len(stations)
    combinations = int((stationCount * (stationCount-1))/2)
    venueID_map = stations.reset_index().set_index("venueID")["index"].to_dict()
    d = np.zeros([combinations, 7*24])

    for index, row in checkins.iterrows():
        s1 = venueID_map[row["StartStationID"]]
        s2 = venueID_map[row["EndStationID"]]
        date = datetime.fromtimestamp(row["StartDate"])
        timestamp = date.weekday()*24 + date.hour
        if s1 == s2:
            continue
        d[pair_loc(s1, s2, stationCount), timestamp] += 1
    return d
        
def pair_loc(s1,s2, c):
    if s2 < s1:
        s1, s2 = s2, s1
    l = 0
    for i in range(c-s1, c):
        l += i

    l += (s2-s1-1)
    return l

def threshold_pair_data(data, threshold):
    return data[data.max(axis=1) > threshold]

def threshold_pair_df(df, data, threshold):
    df["max"] = pd.Series(data.max(axis=1))
    return df[df["max"] > threshold]

def get_pair_clusters(df, clustered_stations):
    df = df.join(clustered_stations.set_index("venueID")["cluster"], on="S1")
    df.rename(columns={"cluster": "S1c"}, inplace=True)
    df = df.join(clustered_stations.set_index("venueID")["cluster"], on="S2")
    df.rename(columns={"cluster": "S2c"}, inplace=True)
    df.drop(df[df["S1c"].isna()].index, inplace=True)
    df.drop(df[df["S2c"].isna()].index, inplace=True)
    df["S1c"] = pd.to_numeric(df["S1c"], downcast="integer")
    df["S2c"] = pd.to_numeric(df["S2c"], downcast="integer")
    return df

def label_anal(df, l):
    label_pairs = df[df["label"] == l]
    #label_pairs["S1c"].value_counts()
    #label_pairs["S2c"].value_counts()
    cluster_pairs = pd.Series(label_pairs[["S1c","S2c"]].to_string(header=False, index=False).split('\n'))
    print("Most common cluster pairs")
    print(cluster_pairs.value_counts()[0:10].to_string())
    print("Use of each cluster")
    for i in range(0,6):
        print(i)
        print(len(label_pairs[(label_pairs["S1c"] == i) | (label_pairs["S2c"] == i)]))

def plot_cluster(c):
    plt.plot(c)
    plt.xticks(np.arange(0, 168, 24), ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"))
    plt.xlabel("Day")
    plt.ylabel("Activity")    

# Get combinations and create a Dataframe from it
timestamped_data = pair_data(stations, checkins)
station_pairs = list(itertools.combinations(stations["venueID"].values,2))
pairs_df = pd.DataFrame(station_pairs, columns=['S1', 'S2'])

# Perform PCA
pca = PCA(n_components=reduced_components)
reduced_data = pca.fit_transform(timestamped_data)

# Threshold the data
cut_data = threshold_pair_data(reduced_data, threshold)
pairs_df = threshold_pair_df(pairs_df, reduced_data, threshold)

# Need to reset index as some rows have been cut
pairs_df.reset_index(inplace=True)
pairs_df = get_pair_clusters(pairs_df, clustered_stations)

n_data = normalize(cut_data)

# Perfom k-means
model = KMeans(n_clusters=n_clusters)
model.fit(n_data)

pairs_df["label"] = pd.Series(model.labels_)
