import datahandler as dh
from datetime import datetime 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import bpmm 

# Load in the data
stations_a = pd.read_csv("cycledata_LDN_BikePoints.csv")
checkins = pd.read_csv("2015_01-02.csv")

# Get the number of checkins at each station
stations_a["count"] = stations_a["venueID"].map(lambda x: len(checkins[(checkins["EndStationID"] == x) | (checkins["StartStationID"] == x)]))

# Remove any stations with 0 checkins
stations = stations_a[stations["count"] != 0]

# Set a period of 1 week
start_date = datetime(2015, 2, 1)
end_date = datetime(2015, 2, 8)

# Organize the data into a matrix form (#Station x #Days x 48)
T = dh.f(start_date, end_date, checkins, stations)

# Create the model
model = bpmm.BikePoissonMixtureModel(T, start_date, 5)

# Iterate EM steps 10 times
for i in range(0,10):
    model.E_step()
    model.M_step()