from datetime import datetime 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import operator as op
from functools import reduce


# Create the departured part part
def d(startdate, enddate, stationID,checkins):
    startdate = startdate.replace(hour=0,minute=0,second=0)
    enddate = enddate.replace(hour=0,minute=0,second=0)
    # Just make sure we only have the entries for the station
    s = checkins[checkins["StartStationID"] == stationID]
    # Just make sure we only have the entries between start and end date
    s = s[s["StartDate"].between(startdate.timestamp(), enddate.timestamp()+86399)]
    # Get a Series of all the days by dividing for the time for 1 day 
    # and subtracting the start date as an offset
    days = s["StartDate"].map(lambda sd: int(sd/86400)).values
    days = days - int(startdate.timestamp()/86400)
    # Get a series of the hours
    hours = s["StartDate"].map(lambda sd: datetime.fromtimestamp(sd).hour).values
    # Get the number of days
    D = int(enddate.timestamp()/86400) - int(startdate.timestamp()/86400) + 1
    # Create an empty 2d-array
    countseries = np.zeros([D,24])
    # iterrate across all the entries and increment all appropiate entries in the array
    for i in range(0,len(days)):
        countseries[days[i],hours[i]] += 1
    return countseries

# Create the arrivals part
def a(startdate, enddate, stationID,checkins):
    startdate = startdate.replace(hour=0,minute=0,second=0)
    enddate = enddate.replace(hour=0,minute=0,second=0)
    # Just make sure we only have the entries for the station
    s = checkins[checkins["EndStationID"] == stationID]
    # Just make sure we only have the entries between start and end date
    s = s[s["EndDate"].between(startdate.timestamp(), enddate.timestamp()+86399)]
    # Get a Series of all the days by dividing for the time for 1 day 
    # and subtracting the start date as an offset
    days = s["EndDate"].map(lambda sd: int(sd/86400)).values
    days = days - int(startdate.timestamp()/86400)
    # Get a series of the hours
    hours = s["EndDate"].map(lambda sd: datetime.fromtimestamp(sd).hour).values
    # Get the number of days
    D = int(enddate.timestamp()/86400) - int(startdate.timestamp()/86400) + 1
    # Create an empty 2d-array
    countseries = np.zeros([D,24])
    # iterrate across all the entries and increment all appropiate entries in the array
    for i in range(0,len(days)):
        countseries[days[i],hours[i]] += 1
    return countseries

# Create the feature matrix 
def f(startdate, enddate, checkins, stations):
    # Number of stations
    N = len(stations)

    # Number of days
    D = int(enddate.timestamp()/86400) - int(startdate.timestamp()/86400) + 1
    
    # Create empty tensor
    T = np.zeros([N,D,48])
    
    stationIDs = stations["venueID"].values.astype(int)
    for i in range(0, len(stationIDs)) :
        T[i] = np.concatenate([a(startdate,enddate,stationIDs[i],checkins), d(startdate,enddate,stationIDs[i],checkins)],axis=1)

    # The big tensor
    return T 

# Create the nice looking  weekday/weekend x arrivals/departures graph
# p should be model.lmbda
def plot_pattern(p):
    fig = plt.figure()

    # Outside Plot
    ax = fig.add_subplot(111)

    ax11 = fig.add_subplot(221) # Top-left
    ax12 = fig.add_subplot(222) # Top-right
    ax21 = fig.add_subplot(223) # Bottom-left
    ax22 = fig.add_subplot(224) # Bottom right

    # Turn off axis lines and ticks of the big subplot
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.spines['right'].set_color('none')
    ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')

    ax.set_ylabel("Activity")
    ax.set_xlabel("Hour")

    ax11.plot(p[0,0:24])
    ax11.set_title("Arrivals")

    ax12.plot(p[0,24:])
    ax12.set_title("Departures")
    ax12.yaxis.set_label_position("right")
    ax12.set_ylabel("Weekday")

    ax21.plot(p[1,0:24])
    
    ax22.plot(p[1,24:])
    ax22.yaxis.set_label_position("right")
    ax22.set_ylabel("Weekend")
