Requires:
python3
matlabplotlib
pandas
numpy
sklearn

"python -i run.py"              for mixture model
"python -i station_pairs.py"    for pair analysis