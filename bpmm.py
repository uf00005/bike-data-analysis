import numpy as np
from scipy.stats import poisson

class BikePoissonMixtureModel:
    T = 48
    L = 2
    lweekday = 0
    lweekend = 1

    def __init__(self, tensor, startdate, station_clusters):
        self.X = tensor

        # Keep a counter for the iteration 
        self.iter = 0

        # Set the constants 
        self.K = station_clusters
        self.N = tensor.shape[0]
        self.D = tensor.shape[1]
        self.startdate = startdate

        # Initalize Parameters
        self.alpha = np.zeros([self.N])
        DTinv = 1/(self.D * self.T)

        for s in range(0, self.N):
            self.alpha[s] = (self.X[s].sum()*DTinv)

        self.pi = np.random.rand(self.K)
        self.lmbda = np.random.rand(self.K, self.L, self.T)

        self.W = np.zeros([self.D,self.L])
        
        c = startdate.weekday()
        for i in range(0,self.D):
            if (c+i) % 7 < 5:
                self.W[i] = [1,0]
            else:
                self.W[i] = [0,1]
        
        # Keep track of changes in expectation
        self.Earray = []

    def activity_vector_conditional_density(self, station, day, station_cluster):
        s = station
        d = day
        p = 1.0
        k = station_cluster

        l = 0
        if self.W[d, self.lweekday] == 1:
            l = self.lweekday
        else:
            l = self.lweekend

        for t in range(0, self.T):
            p *= poisson.pmf(self.X[s,d,t], self.alpha[s] * self.lmbda[k,l,t])
        
        return p
    
    def E_step(self):
        t = np.zeros([self.N, self.K])
        E = 0.0
        zeroeEs = 0

        # Calculate tsk, the a posteriori
        for s in range(0,self.N):
            if (s % int(self.N/10)) == 0:
                print("s = ", s)

            # Denominator of t is a sum of all numerators
            den = 0.0
            for k in range(0, self.K):
                num = 1.0
                for d in range(0, self.D):
                    num *= self.activity_vector_conditional_density(s,d,k)

                num *= self.pi[k]   
                t[s,k] = num
                den += num
            
            if den == 0:
                t[s] = 0.0
            else:
                stationE = np.log10(t[s])

                t[s] = np.divide(t[s], den)

                stationE *= t[s]
                if np.isnan(stationE).any():
                    zeroeEs += 1
                E += np.nansum(stationE)

        self.Earray.append(E)
        print("Expectation = ", E)
        print("zeroeEs = ", zeroeEs)
        self.t = t

    def M_step(self):
        # Update parameters
        # pi
        print("Updating pi")
        for k in range(1, self.K):
            self.pi[k] = (self.t[:,k].sum())/self.N

        # lambda
        print("Updating lambda")
        for k in range(0, self.K):
            for l in range(0, self.L):
                for t in range(0, self.T):
                    num = 0
                    for s in range(0,self.N):
                        for d in range(0, self.D):
                            num += self.t[s,k] * self.W[d,l] * self.X[s,d,t]
                    
                    den = 0.0
                    for s in range(0, self.N):
                        den += self.t[s,k] * self.alpha[s] * self.W[:,l].sum()

                    self.lmbda[k,l,t] = (num/den)

